﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ArenaSim
{
    public class ArenaData
    {
        ///<summary>
        ///Collects data from a given json file
        ///</summary>
        public List<Data> AData()
        {
            //Pass the file
            string json = File.ReadAllText("all-collectibles.json");
            //Initialize
            var serializer = new JavaScriptSerializer();
            //Takes input from json and converts to the given class
            return serializer.Deserialize<List<Data>>(json);
        }
        ///<summary>
        ///Makes a collection of all the cards that are relevant to particualr class
        ///</summary>
        public List<Data> SelectionOfCards(List<Data> cardData, string hero)
        {
            var classCardList = new List<Data>();
            foreach (Data item in cardData)
            {
                if (item.hero == hero.ToLower() || item.hero.Equals("neutral"))
                {
                    classCardList.Add(item);
                }
            }
            return classCardList;
        }
        ///<summary>
        ///Put items in a random order
        ///</summary>
        public List<string> Randomizer(List<string> data)
        {
            var rand = new Random();
            var shuffledItems = data.OrderBy(x => rand.Next()).ToList();
            return shuffledItems;
        }

        ///<summary>
        ///Gives a calculated rarity
        ///</summary>
        public string Probability(int i)
        {
            var rand = new Random();
            int number = 0;
            int counter = 0;
            if (i == 1 || i == 10 || i == 20 || i == 30)
                counter = 1;
            number = rand.Next(1, 101);
            while (80 <= number && counter<3)
            {
                number = rand.Next(1, 101);
                counter++;
            }
            return quality[counter];
        }

        public static List<string> classes = new List<string> { "Mage", "Rogue", "Shaman", "Paladin", "Warrior", "Warlock", "Hunter", "Priest" };
        public static List<string> quality = new List<string> {"free, common", "rare", "epic", "legendary"};
    
}
    public class Data
    {
        public string name { get; set; }
        public string hero { get; set; }
        public string category { get; set; }
        public string quality { get; set; }
        public string description { get; set; }
        public string mana { get; set; }
        public string attack { get; set; }
        public string health { get; set; }


    }
}
