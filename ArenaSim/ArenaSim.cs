﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArenaSim
{
    public class ArenaSim
    {
        
        public static void Main(string[] args)
        {
            var checker = new InputManager();
            var arenadata = new ArenaData();
            var cardData = arenadata.AData();
            int consoleFiller = (int)(Console.BufferWidth/2);
        
            Console.WriteLine("Welcome to the Arena Simulator!");

            //Shuffles possible classes
            var shuffledClasses = arenadata.Randomizer(ArenaData.classes);

            Console.WriteLine($"Pick a class using the given number: 1. {shuffledClasses[0]}, 2. {shuffledClasses[1]} or 3. {shuffledClasses[2]}");

            //Checks input
            int choiceNum = checker.Manage(cardData);
            string chosenClass = shuffledClasses[choiceNum-1];
            Console.WriteLine($"\nCongratz, you chose {chosenClass} ");

            //Selects all the appropriate cards
            List<Data> relevantCards = arenadata.SelectionOfCards(cardData, chosenClass);
            List<Data> deck = new List<Data>();
            
            //Initalizing card picker
            for (int i = 0; i < 30; i++)
            {
                //Safety emptying
                string randQuality;
                List<string> sameQuality = new List<string>();
                List<string> shuffledCards = new List<string>();
                List<string> firstThree = new List<string>();

                //Picking random quality
                randQuality = arenadata.Probability(i+1);
                //Making a list of same quality cards
                foreach (Data item in relevantCards)
                {
                    if (randQuality.Contains(item.quality.ToLower()))
                    {
                        int counter = 0;

                        //Checking duplicates
                        foreach (Data card in deck)
                        {
                            if (card.name.Equals(item.name))
                            {
                                counter++;                                
                            }
                        }
                        if (item.quality!="legendary" && counter <= 1)
                        {
                            sameQuality.Add(item.name);
                        }
                        if (item.quality == "legendary" && counter <= 0)
                        {
                            sameQuality.Add(item.name);
                        }
                    }                    
                }
                //Shuffling same quality cards
                shuffledCards = arenadata.Randomizer(sameQuality);

                for (int j = 0; j<3; j++)
                {
                    firstThree.Add(shuffledCards[j]);
                }

                Console.WriteLine($"\nPick your {i + 1}. card:");
                Console.WriteLine($"\n1. {firstThree[0]} 2. {firstThree[1]}, 3. {firstThree[2]}");

                choiceNum = checker.Manage(cardData, firstThree);
                Console.WriteLine("\nYou chose " + firstThree[choiceNum - 1]+"\n");
                Console.WriteLine(string.Concat(Enumerable.Repeat(@"|-", consoleFiller)));

                //Connect name back with its data
                foreach (Data card in relevantCards)
                {
                    if (firstThree[choiceNum-1].Equals(card.name))
                    {
                        deck.Add(card);
                        break;
                    }
                }                
            }

            Console.WriteLine("Your draft overview:");
            using (StreamWriter sw = new StreamWriter(@"C:\Users\kasutaja\Desktop\ArenaDraft.txt"))
            foreach (Data card in deck)
            {
                Console.WriteLine(card.name);
                    sw.WriteLine(card.name);
            }
            


            //Console.WriteLine($"\n\n{chosenClass}'s cards and neutrals are as follows: ");

            //foreach (Data item in relevantCards)
            //{
            //    Console.WriteLine(item.quality);
            //}

            //Console.WriteLine("\n\nHere is an example card: " + cardData[5].name);
        }
    }
}
